"use strict";

let addressRef,useAddressRef,roomNumberRef,lightsRef,heatingCoolingRef,seatsUsedRef,totalSeatsRef,addressAuto;

    addressRef = document.getElementById('address');
    useAddressRef = document.getElementById('useAddress');
    roomNumberRef = document.getElementById('roomNumber');
    lightsRef = document.getElementById('lights');
    heatingCoolingRef = document.getElementById('heatingCooling');
    seatsUsedRef = document.getElementById('seatsUsed');
    totalSeatsRef = document.getElementById('seatsTotal');




//Reverse Geolocation
let reverseGeo = function(data){
    addressAuto = data.results[0].formatted;
    let parts = addressAuto.split(',');
    
  
    
    document.getElementById('theAddress').innerHTML = ` <tr>
                                        <td>
                                            <div class="mdl-textfield mdl-js-textfield" id='theAddress'>
                                                <input class="mdl-textfield__input" type="text" id="address">
                                                <label class="mdl-textfield__label" for="address" id="address"></label>
                                            </div>
                                        </td>
                                    </tr>`;
    
    document.getElementById('address').value = parts[0];
    


} 

//Reverse Geolocation getting the lat and long  
function trackLocation(){
    
    
 let myPosition =[];
    let lang,lat;
        
        navigator.geolocation.getCurrentPosition(position => {
        lang = position.coords.latitude;
        lat = position.coords.longitude;
   
   
    
 let apiKey = '9fca42448e534f3c974ffea6192798c9';

    let api_url = 'https://api.opencagedata.com/geocode/v1/json';
    let request_url = api_url  + '?'+'&jsonp=reverseGeo'+ '&q=' + encodeURIComponent(lang + ',' + lat)+ '&key=' + apiKey +'&pretty=1';
    
      
    let script = document.createElement('script');
    script.src = request_url;
    document.getElementsByTagName('body')[0].appendChild(script);
    });

   
}






//Saving the filled form 
function save(){


let address,useAddress,roomNumber,lights,heatingCooling,seatsUsed,totalSeats,timeCheck;
    
    address = addressRef.value.trim();
    useAddress = useAddressRef.checked;
    roomNumber = roomNumberRef.value.trim();
    lights = lightsRef.checked;
    heatingCooling = heatingCoolingRef.checked;
    seatsUsed = Number(seatsUsedRef.value);
    totalSeats = Number(totalSeatsRef.value);
    timeCheck = new Date();
  
    
  
    if(useAddress === true){
        address = addressAuto;
    
    }
   

    if((typeof(address) === 'string' && address !== "" && typeof(roomNumber) === 'string' && typeof(seatsUsed)==='number' && isNaN(seatsUsed) === false && typeof(totalSeats)==='number' && isNaN(totalSeats) === false)){
         
        let room = new RoomUsage(roomNumber,address,lights,heatingCooling,seatsUsed,totalSeats,timeCheck);
        
       
        newRoom.addRoom(room);
       
        

           displayMessage('Data Saved', 1500)
     

    
       
    } else {
           document.getElementById('errorMessages').innerText = 'Inccorect Input'
       }

    
    localStorage.setItem(storageKey,JSON.stringify(newRoom));
    
    //Pop-up message after saved before page is reloaded
    setTimeout(()=>{
        window.location.reload();
    }, 700);
    
    
}





//Retriving Data 
window.addEventListener('load',loadData);





//Resetting the whole page by reload 
function reset(){
      
    
window.location.reload();
}

