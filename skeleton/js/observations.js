"use strict";












let FullHTML = "";

function displayGetRoom(){
   for(let i =0; i<newRoom.Room.length; i++){


    let myLights = lightsOn(newRoom.Room[i]._lightsOn);
    let myHeatingCooling = heatingCooling(newRoom.Room[i]._heatingCoolingOn);
    let id = `content${i}`;
    //Get Address 
    let parts = newRoom.Room[i]._address.split(',');
    //Month
    let parts2 = newRoom.Room[i]._timeChecked.split('-');
    //Date
    let parts3 = parts2[2].split('T');
    //Time 
    let parts4 = parts3[1].split('.');
    
    //Writing
        FullHTML += `
                    <div class="mdl-cell mdl-cell--4-col" id=${id}>
                   
                        <table class="observation-table mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                            <thead>
                                <tr><th class="mdl-data-table__cell--non-numeric">
                                    <h4 class="date">${parts3[0]}/${parts2[1]}.</h4>
                                    <h4>
                                       ${parts[0]}<br />
                                        Rm ${newRoom.Room[i]._roomNumber}
                                    </h4>
                                </th></tr>
                            </thead>
                            <tbody>
                                <tr><td class="mdl-data-table__cell--non-numeric">
                                    Time: ${parts4[0]} <br /> 
                                    Lights: ${myLights}<br />
                              
                                    Heating/cooling:${myHeatingCooling}<br />
                                    Seat usage: ${newRoom.Room[i]._seatsUsed} / ${newRoom.Room[i]._seatsTotal}<br/ >
                                    <button class="mdl-button mdl-js-button mdl-button--icon" onclick="deleteObservationAtIndex(${i})"> 
                                        <i class="material-icons">delete</i>
                                    </button>
                                </td></tr>
                            </tbody>
                        </table>
                    </div>

`
    
    } 
}

function displayRoom(){
    contentRef.innerHTML += FullHTML;
}

//Get all room
displayGetRoom();

//Run function to display Room 
displayRoom();

function indexList(arr,val){
     let indexArr = [];
  
    for(let i=0;i<arr.length;i++){

        if(arr[i]._roomNumber.toLowerCase() === val || arr[i]._address.split(',')[0].toLowerCase() === val){

           indexArr.push(i);
        }
    }
    return indexArr;
}



function displaySearchList(){
    
    
    let text = document.getElementById("searchField").value;
    let useText = text.toLowerCase();
    let theIndex = indexList(newRoom.Room,useText);


    
    
    
    contentRef.innerHTML = ``;
    
    if(useText !== ""){
        
        for(let j =0;j<theIndex.length;j++){
            let i = theIndex[j];
            

            let myLights = lightsOn(newRoom.Room[i]._lightsOn);
            let myHeatingCooling = heatingCooling(newRoom.Room[i]._heatingCoolingOn);
            let id = `content${i}`;

            //Address
            let parts = newRoom.Room[i]._address.split(',');
            //Month
            let parts2 = newRoom.Room[i]._timeChecked.split('-');
            //Date
            let parts3 = parts2[2].split('T');
            //Time 
            let parts4 = parts3[1].split('.');


            contentRef.innerHTML += `
                            <div class="mdl-cell mdl-cell--4-col" id=${id}>

                                <table class="observation-table mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                                    <thead>
                                        <tr><th class="mdl-data-table__cell--non-numeric">
                                            <h4 class="date">${parts3[0]}/${parts2[1]}.</h4>
                                            <h4>
                                               ${parts[0]}<br />
                                                Rm ${newRoom.Room[i]._roomNumber}
                                            </h4>
                                        </th></tr>
                                    </thead>
                                    <tbody>
                                        <tr><td class="mdl-data-table__cell--non-numeric">
                                            Time: ${parts4[0]} <br />
                                            Lights: ${myLights}<br />
                                          
                                            Heating/cooling:${myHeatingCooling}<br />
                                            Seat usage: ${newRoom.Room[i]._seatsUsed} / ${newRoom.Room[i]._seatsTotal}<br/ >
                                            <button class="mdl-button mdl-js-button mdl-button--icon" onclick="deleteObservationAtIndex(${i})"> 
                                                <i class="material-icons">delete</i>
                                            </button>
                                        </td></tr>
                                    </tbody>
                                </table>
                            </div>

        `

                }
    
    }
    
    //if empty string input on the search field 
    if(useText === ""){
        
        contentRef.innerHTML = `  <div class="mdl-cell mdl-cell--4-col">
                        <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                            <tbody>
                                <tr><td class="mdl-data-table__cell--non-numeric">Room Observations</td></tr>
                            </tbody>
                        </table>
                    </div>`;
        displayRoom();
        
    }
    
    
  }



//reload after every delete
document.addEventListener("event", function(){
    window.location.reload();
});




