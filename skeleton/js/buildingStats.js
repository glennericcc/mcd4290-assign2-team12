"use strict";


let buildingPropertyName = [];
let numberOfObservations,numberOfWasteFullObservations,averageSeats,avgLights,avgHeatCool;

//Getting all the property names
for(let x in buildingBucket ){
   buildingPropertyName.push(x);
}
console.log(buildingPropertyName)

//function to calculate amount of wasterfull observations
function wasteFullObservations(object){
    let numberOfWasteFullObservations = 0;
    for(let i =0;i<object.length;i++){
        if(object[i]._heatingCoolingOn === true && object[i]._seatsUsed === 0){
            numberOfWasteFullObservations++
            }
    }
    
    
    
    return numberOfWasteFullObservations;
}



//function to calculate average seats 
function averageSeatsOfObvservation(object){
    
    let averageSeats;
    let totalSeatsObs=0;
    let usedSeatsObs=0;
    
    for(let i = 0;i<object.length;i++){
       usedSeatsObs += object[i]._seatsUsed;
        totalSeatsObs += object[i]._seatsTotal;
    }
    averageSeats = ((usedSeatsObs/totalSeatsObs)*100).toFixed(2);
    return averageSeats;
}

//Average Lights Usage
function averageLights(object){
    let lights = 0;
    let avgLights;
    
    for(let i =0;i<object.length;i++){
    if(object[i]._lightsOn === true){
        lights++
        }

    }
    
    avgLights= (lights/numberOfObservations).toFixed(2);
    return avgLights;
}

//Average Heating and Cooling 
function averageHeatingCooling(object){
   
    let heatCool = 0;
    let avgHeatCool;
    
    for(let i=0;i<object.length;i++){
        if(object[i]._heatingCoolingOn === true){
            heatCool++
        }
    
    }
    avgHeatCool = (heatCool/numberOfObservations).toFixed(2);
    return avgHeatCool;
}

let HTML = "";
//To display the observations based on the Buckets
for(let i=0;i<buildingPropertyName.length;i++){
        let currentBuilding= buildingBucket[buildingPropertyName[i]]
   
        
        numberOfObservations = currentBuilding.length;
        numberOfWasteFullObservations = wasteFullObservations(currentBuilding);
        averageSeats = averageSeatsOfObvservation(currentBuilding);
        avgLights = averageLights(currentBuilding);
        avgHeatCool = averageHeatingCooling(currentBuilding);
        
    if(numberOfWasteFullObservations > 0){    
    HTML += `
    
    
    <div class="mdl-cell mdl-cell--4-col">
                        <table class="observation-table mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                            <thead>
                                <tr><th class="mdl-data-table__cell--non-numeric" style="background-color:Red;">
                                    <h4>
                                       ${buildingPropertyName[i].split(',')[0]}
                                    </h4>
                                </th></tr>
                            </thead>
                            <tbody>
                                <tr><td class="mdl-data-table__cell--non-numeric">
                                    Observations:${numberOfObservations}<br />
                                    Wasteful observations:${numberOfWasteFullObservations}<br />
                                    Average seat utilisation: ${averageSeats}%<br />
                                    Average lights utilisation: ${avgLights}%<br />
                                    Average heating/cooling utilisation:${avgHeatCool}%
                                </td></tr>
                            </tbody>
                        </table>
                    </div>
`
    }else{
          HTML += `
                    
    <div class="mdl-cell mdl-cell--4-col">
                        <table class="observation-table mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                            <thead>
                                <tr><th class="mdl-data-table__cell--non-numeric">
                                    <h4>
                                       ${buildingPropertyName[i].split(',')[0]}
                                    </h4>
                                </th></tr>
                            </thead>
                            <tbody>
                                <tr><td class="mdl-data-table__cell--non-numeric">
                                    Observations:${numberOfObservations}<br />
                                    Wasteful observations:${numberOfWasteFullObservations}<br />
                                    Average seat utilisation: ${averageSeats}%<br />
                                    Average lights utilisation: ${avgLights}%<br />
                                    Average heating/cooling utilisation:${avgHeatCool}%
                                </td></tr>
                            </tbody>
                        </table>
                    </div>
`
    }

    
}
document.getElementById('content').innerHTML += HTML;