"use strict";

function number2str(element){
    
    let convert = [];
    for(let i=0;i<element.length;i++){
        convert.push(Number(element[i]));
    }
    return convert;
}

function compare(element,original){
        let indexCompare = [];
    for(let i =0;i<element.length;i++){
        indexCompare.push(original.indexOf(element[i]));
        
    }
    return indexCompare;
}



let HTML = "";



//for 8 and 9 

for(let i = 8;i<10;i++){
  let parts2,parts3,parts4;
    
    HTML += `  <div class="mdl-cell mdl-cell--4-col">
                        <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                            <thead>
                                <tr><th class="mdl-data-table__cell--non-numeric">
                                    <h5>Worst occupancy for ${i}:00</h5>
                                </th></tr>
                            </thead>
                            <tbody>`;
    
    
   if(timeBucket.hasOwnProperty(`0${i}`)){
       
        let occupancy= [];
        let address = [];
        let myLights = [];
        let myHeatingCooling = [];
        let roomNumber = [];
        let day = [];
        let month = [];
        let year = [];
        let time = [];
        let occupancySort = [];
        let occupancyNotSort = [];
        let indexCompare;
       
        for(let j=0;j<timeBucket[`0${i}`].length;j++){
       
            //Month
            parts2 =timeBucket[`0${i}`][j]._timeChecked.split('-');
            //Date
            parts3 = parts2[2].split('T');
            //Time 
            parts4 = parts3[1].split('.');
       
        
            //Results 
            day.push(parts3[0]);
            month.push(parts2[1]);
            year.push(parts2[0]);
            time.push(parts4[0]);
         
            
        
            
             myLights.push(lightsOn(timeBucket[`0${i}`][j]._lightsOn));
             myHeatingCooling.push(heatingCooling(timeBucket[`0${i}`][j]._heatingCoolingOn));
             
             let theAddress = timeBucket[`0${i}`][j]._address.split(',')
             address.push(theAddress[0]);
            
             roomNumber.push(timeBucket[`0${i}`][j]._roomNumber);
            
             let percentageOccupancy =((timeBucket[`0${i}`][j]._seatsUsed/timeBucket[`0${i}`][j]._seatsTotal)*100).toFixed(2);
            
             occupancy.push(percentageOccupancy);
            
            
        }
        
       //occupancy before sorted 
        occupancyNotSort = number2str(occupancy);
        
       //sorted occupancy 
        occupancySort = number2str(occupancy.sort((a,b)=>a-b));
       //Function to get the index of the original value 
        indexCompare = compare(occupancySort,occupancyNotSort );
        

        if(occupancy.length > 5){

            for(let k = 0;k<5;k++){

                HTML += `  <tr><td class="mdl-data-table__cell--non-numeric" >
                                <div><b>${address[indexCompare[k]]}; ${roomNumber[indexCompare[k]]}</b></div>
                                <div>Occupancy: ${occupancySort[k]}</div></br>
                                <div>Heating/cooling: ${myHeatingCooling[indexCompare[k]]}</div>
                                <div>Lights: ${myLights[indexCompare[k]]}</div>
                                <div><font color="grey">
                                        <i> ${day[indexCompare[k]]}/${month[indexCompare[k]]}/${year[indexCompare[k]]}, ${time[indexCompare[k]]} </i>
                                </font></div>
                            </td></tr>` ;                  

            }

         }else{

            for(let k=0;k<dummyAray.length;k++){

                                    HTML += `  <tr><td class="mdl-data-table__cell--non-numeric">
                                <div><b>${address[k]}; ${roomNumber[k]}</b></div>
                                <div>Occupancy: ${occupancy[k]} </div></br>
                                <div>Heating/cooling: ${myHeatingCooling[k]}</div>
                                <div>Lights: ${myLights[k]}</div>
                                <div><font color="grey">
                                        <i> ${day[indexCompare[k]]}/${month[indexCompare[k]]}/${year[indexCompare[k]]}, ${time[indexCompare[k]]} </i>
                                </font></div>
                            </td></tr>` ;                    

                }

            }

        }

    else {
          HTML += `  <tr><td class="mdl-data-table__cell--non-numeric">
                        <div><b>No Observations Recorded</b></div>

                    </td></tr>` ;   
    }

    HTML +=`          </tbody>
                        </table>
                    </div>
            `

    document.getElementById('content').innerHTML += HTML;
    HTML = "";
}



//for 10-18
for(let i = 10;i<19;i++){
  let parts2,parts3,parts4;
    
    HTML += `  <div class="mdl-cell mdl-cell--4-col">
                        <table class="mdl-data-table mdl-js-data-table mdl-shadow--2dp">
                            <thead>
                                <tr><th class="mdl-data-table__cell--non-numeric">
                                    <h5>Worst occupancy for ${i}:00</h5>
                                </th></tr>
                            </thead>
                            <tbody>`;
    
    
   if(timeBucket.hasOwnProperty(`${i}`)){
       
        let occupancy= [];
        let address = [];
        let myLights = [];
        let myHeatingCooling = [];
        let roomNumber = [];
        let day = [];
        let month = [];
        let year = [];
        let time = [];
        let occupancySort = [];
       let occupancyNotSort = [];
       let indexCompare;
       
        for(let j=0;j<timeBucket[`${i}`].length;j++){
       
            //Month
            parts2 =timeBucket[`${i}`][j]._timeChecked.split('-');
            //Date
            parts3 = parts2[2].split('T');
            //Time 
            parts4 = parts3[1].split('.');
       
        
        //Results 
        day.push(parts3[0]);
        month.push(parts2[1]);
        year.push(parts2[0]);
        time.push(parts4[0]);
         
            
        
            
             myLights.push(lightsOn(timeBucket[`${i}`][j]._lightsOn));
             myHeatingCooling.push(heatingCooling(timeBucket[`${i}`][j]._heatingCoolingOn));
             
             let theAddress = timeBucket[`${i}`][j]._address.split(',')
             address.push(theAddress[0]);
            
             roomNumber.push(timeBucket[`${i}`][j]._roomNumber);
            
             let percentageOccupancy =((timeBucket[`${i}`][j]._seatsUsed/timeBucket[`${i}`][j]._seatsTotal)*100).toFixed(2);
            
             occupancy.push(percentageOccupancy);
            
            
        }
        
       //occupancy before sorted 
        occupancyNotSort = number2str(occupancy);
        
       //sorted occupancy 
        occupancySort = number2str(occupancy.sort((a,b)=>a-b));
        indexCompare = compare(occupancySort,occupancyNotSort );
        
     
       
        
       
       
        
                    if(occupancy.length > 5){

            for(let k = 0;k<5;k++){

                HTML += `  <tr><td class="mdl-data-table__cell--non-numeric" >
                                <div><b>${address[indexCompare[k]]}; ${roomNumber[indexCompare[k]]}</b></div>
                                <div>Occupancy: ${occupancySort[k]}</div></br>
                                <div>Heating/cooling: ${myHeatingCooling[indexCompare[k]]}</div>
                                <div>Lights: ${myLights[indexCompare[k]]}</div>
                                <div><font color="grey">
                                        <i> ${day[indexCompare[k]]}/${month[indexCompare[k]]}/${year[indexCompare[k]]}, ${time[indexCompare[k]]} </i>
                                </font></div>
                            </td></tr>` ;                  

            }

         }else{

            for(let k=0;k<dummyAray.length;k++){

                                    HTML += `  <tr><td class="mdl-data-table__cell--non-numeric">
                                <div><b>${address[k]}; ${roomNumber[k]}</b></div>
                                <div>Occupancy: ${occupancy[k]} </div></br>
                                <div>Heating/cooling: ${myHeatingCooling[k]}</div>
                                <div>Lights: ${myLights[k]}</div>
                                <div><font color="grey">
                                        <i> ${day[indexCompare[k]]}/${month[indexCompare[k]]}/${year[indexCompare[k]]}, ${time[indexCompare[k]]} </i>
                                </font></div>
                            </td></tr>` ;                    

                }

            }
        
            }
            
        else {
                      HTML += `  <tr><td class="mdl-data-table__cell--non-numeric">
                                    <div><b>No Observations Recorded</b></div>
                                  
                                </td></tr>` ;   
        }
        
     HTML +=`          </tbody>
                        </table>
                    </div>
            `
     
    document.getElementById('content').innerHTML += HTML;
    HTML = "";
    }







