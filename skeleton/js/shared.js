"use strict";




//Bucket Time 

//ROOMUSAGE CLASS 
class RoomUsage {
    constructor(roomNumber,address,lightsOn,heatingCoolingOn,seatsUsed,seatsTotal,timeChecked){
        
        if(roomNumber !== "" && typeof(roomNumber) === "string"){
        this._roomNumber = roomNumber;
        }
        
          
        if(address!== "" && typeof(address) === "string"){
        this._address = address;
        }
        
        this._lightsOn = lightsOn;
        this._heatingCoolingOn = heatingCoolingOn;
      
        
        
      
            this._seatsUsed = seatsUsed;
        
        
    
            this._seatsTotal = seatsTotal;
        

        this._timeChecked = timeChecked;
    }
    
    
    get roomNumber(){
         return this._roomNumber;   
    }
    
    get address(){
        return this._address;
    }

    get lightsOn(){
        return this._lightsOn;
    }
    
    get heatingCoolingOn(){
        return this._heatingCoolingOn;
    }
    
    get seatsTotal(){
        return this._seatsTotal;
    }
    
    get timeChecked(){
        return this._timeChecked;
    }
    
      set roomNumber(element){
         return this._roomNumber= element;  
    }
    
   set address(element){
        return this._address= element;
    }

   set lightsOn(element){
        return this._lightsOn= element;
    }
    
   set heatingCoolingOn(element){
        return this._heatingCoolingOn= element;
    }
    
    set seatsTotal(element){
        return this._seatsTotal= element;
    }
    
    set timeChecked(element){
        this._timeChecked = element;
    }
    toString(){
        return `Room Number :  ${this._roomNumber}, Address : ${this._address}, Lights On : ${this._lightsOn}, Heating Cooling On : ${this._heatingCoolingOn}, Total Seats : ${this._seatsTotal}, Time Checked : ${this._timeChecked}`
    }

}

class RoomUsageList{
    constructor(){
        this._roomList = [];
   
        
    }
    
    addRoom(roomObj){
        this._roomList.unshift(roomObj);
    }
    
    get Room(){
        return this._roomList;
    }
    

    
    setNewRoom(element){
        this._roomList = element;
    }
    
 
    
}

let newRoom = new RoomUsageList();


//conetent ID ref
const contentRef = document.getElementById('content');

//Storage key for storing data
const storageKey = 'ENG1003-RoomUseList';

newRoom =  loadData();

//Function to get JSON Data 
function loadData(){
    let loadedRoom = JSON.parse(localStorage.getItem(storageKey));
    let myLoadedRoom = new RoomUsageList();
    myLoadedRoom.setNewRoom(loadedRoom._roomList);
    newRoom = myLoadedRoom;
    return newRoom;
    
}



//Deleting observations
function deleteObservationAtIndex(index){
   document.getElementById(`content${index}`).remove();
   newRoom.Room.splice(index,1);
   
   
    localStorage.setItem(storageKey,JSON.stringify(newRoom));
   
  
    
    //reload everytime this function is called
    let event = new CustomEvent("event", { "detail": "Reset Function Value" });
    document.dispatchEvent(event);

    

    
   
}




//Bucket Time 
let timeBucket = {
    
};

//Bucket Address 
let buildingBucket = {
    
};




function lightsOn(element){
    if(element === true){
        return 'On';
    }else{
        return 'Off';
    }
}

function heatingCooling(element){
    if(element === true){
        return 'On';
    }else{
        return 'Off';
    }
}

    
    
 for(let i =0; i<newRoom.Room.length; i++){
        
    //Month
    let parts2 = newRoom.Room[i]._timeChecked.split('-');
   
    //Date
    let parts3 = parts2[2].split('T');
    //Time 
    let parts4 = parts3[1].split('.');
    //Hour
    let parts5 = parts4[0].split(':');
    
    let myPropertyNameTime = parts5[0].toString();
    let myPropertyNameAddress = newRoom.Room[i]._address.split(',')[0];
       
    //Bucketing Time 
    if(timeBucket.hasOwnProperty(myPropertyNameTime)){
        timeBucket[myPropertyNameTime].push(newRoom.Room[i]); 
    }else{
        timeBucket[myPropertyNameTime] =[];
          timeBucket[myPropertyNameTime].push(newRoom.Room[i]); 
    
    }
        
    //Bucketing Address 
    if(buildingBucket.hasOwnProperty(myPropertyNameAddress)){
       buildingBucket[myPropertyNameAddress].push(newRoom.Room[i]); 
    }else{
        buildingBucket[myPropertyNameAddress] =[];
        buildingBucket[myPropertyNameAddress].push(newRoom.Room[i]); 
    
    }
}
    
    









